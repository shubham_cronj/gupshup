var express = require('express');
var app = express();
var http = require('http').createServer(app);

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io1 = require('socket.io').listen(server);
var io = io1.of('/chat');

users = [];
connections = [];
var roomNames = "";
server.listen(process.env.PORT || 3000);
console.log("Running");

app.use(express.static("public"));
app.get('/', function(req,res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection',function (socket) {
    connections.push(socket.id);
    socket.on('disconnect',function (data) {
        // if(!socket.username) return;
        users.splice(users.indexOf(socket.username),1);
        updateUsername();
        connections.splice(connections.indexOf(socket.id),1);
    });

    socket.on('new user', function (data, callback) {
        if(users.indexOf(data)>= 0)
        {
            callback(false);
        }
        else {
            callback(true);
            socket.username = data;
            users.push(socket.username);
                updateUsername();
        }
    });
    function updateUsername() {
        socket.on('giveUserList',function (data) {
            io.emit('get users', users);
        });
        io.emit('get users', users);
    }
    socket.on('oneToOne',function (data) {
        io.to(connections[users.indexOf(data.to)]).emit('msg', data);
    });
    socket.on('createRoom',function (data) {
         socket.join(data);
         roomNames = data;
         socket.emit('joined', 'success');

    });
    socket.on('joinRoom',function (data) {

        var id = connections[users.indexOf(data.add)];
        io.connected[id].join(data.roomName);
        io.connected[id].emit('joined', 'success');
    });
    socket.on('toRoom',function (data) {
        socket.broadcast.to(data.to).emit('fromroom',data);
    });
    socket.on('giveRoomList',function () {
        socket.emit('get rooms', { list: socket.rooms, id:socket.id});
    });
    socket.on('giveParticipants',function (data) {
        var participants = [];
        for(var socketid in io.adapter.rooms[data].sockets)
        {
            if(io.adapter.rooms[data].sockets.hasOwnProperty(socketid))
            {
                participants.push(users[connections.indexOf(socketid)]);
            }
        }
        socket.emit('participants',participants);
    });
});