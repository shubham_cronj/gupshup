app.controller('chat', function ($scope,id,messages,badges,$state) {
    var chatThis = this;
    $scope.username = id.username;
    $scope.block = 'none';
    $scope.showHide = { 'display': 'none'};
    $scope.mode = 'Online Users';
    $scope.addOrDone = '+';
    $scope.showInputName = { 'display': 'none'};
    $scope.rooms = [];
    $scope.participants = "";
    socket.emit('giveUserList',"hi");
    var sendToGroup = false;

    var displayList = new Array();
    var displayRooms = new Array();
    socket.on('get users', function (data) {
        updateOnlineUsers(data);
    });
    socket.on('joined',function () {
        socket.emit('giveRoomList',"hi");
    });
    socket.on('get rooms', function (data) {
        console.log(data);
        displayRooms = [];
        for(var room in data.list)
        {
            if(data.list.hasOwnProperty(room))
            {   if(room != data.id)
                displayRooms.push(room);
            }
        }
        updateRooms();
    });
    function updateOnlineUsers(data) {
        $scope.$apply(function () {
            displayList = data;
            $scope.users = displayList;
        });
    }
    function updateRooms() {
        $scope.$apply(function () {
            // displayRooms = data;
            $scope.rooms = displayRooms;
        });
    }
    $scope.openChat = function(friendName) {
        console.log($scope.rooms);
        if($scope.mode == 'Online Users'){
            $scope.friendName = friendName;
            $scope.msglist = messages[$scope.friendName];
            badges[friendName] = null;
            $scope.showHide = { 'display': 'block'};
            if($scope.rooms.indexOf(friendName)>=0)
            {
                $scope.path = "images/group.jpg";
                socket.emit('giveParticipants', friendName);
                console.log(friendName);
            }
            else
                $scope.path = "images/dp1.png";
        }
        else
            addToRoom(friendName);
    }
    function addToRoom(friendName) {
        socket.emit('joinRoom',{roomName:$scope.groupName, add:friendName});
        $scope.friendName += ' +'+friendName;
    }
    socket.on('msg',function(data) {
        if(messages[data.from])
        {
            messages[data.from].push({msg:data.msg, from: "othersMessage"});
        }
        else {
            messages[data.from]  = new Array();
            messages[data.from].push({msg:data.msg, from: "othersMessage"});
        }
        $scope.$apply(function () {
            $scope.msglist = messages[$scope.friendName];
        });
        if($scope.friendName != [data.from])
        {
            if(badges[data.from]) {
                badges[data.from]++;
            }
            else {
                badges[data.from] = 1;
            }
            $scope.$apply(function () {
                $scope.badges = badges;
            });
        }
    });
    $scope.sendMessage = function () {
        if($scope.msgToSend) {
            if($scope.rooms.indexOf($scope.friendName) < 0) {
                socket.emit('oneToOne', {from: $scope.username, to: $scope.friendName, msg: $scope.msgToSend});
            }
            else {
                socket.emit('toRoom', {msg: $scope.msgToSend, from: id.username, to: $scope.friendName});
                console.log('to room');
            }
            if (messages[$scope.friendName]) {
                messages[$scope.friendName].push({msg: $scope.msgToSend, from: "myMessage"});
            }
            else {
                messages[$scope.friendName] = new Array();
                messages[$scope.friendName].push({msg: $scope.msgToSend, from: "myMessage"});
            }
            $scope.msglist = messages[$scope.friendName];
            $scope.msgToSend = "";
        }

    }
    var currentRoom = "";
    $scope.initiateGroupChat = function () {
        // $state.go('groupchat');
        if($scope.addOrDone == '+'){
            $scope.mode = 'Select Users';
            $scope.addOrDone = 'Done';
            $scope.showInputName = { 'display': 'block'};
            if($scope.rooms.indexOf($scope.friendName)>=0)
            {
                $scope.groupName = $scope.friendName;
                $scope.continueSelect();
            }

        }

        else
        {
            $scope.addOrDone = '+';
            $scope.mode = 'Online Users';
            $scope.showInputName = { 'display': 'none'};
            $scope.friendName = currentRoom;
            socket.emit('giveParticipants',currentRoom);
        }
    }
    $scope.continueSelect = function () {
        if($scope.rooms.indexOf($scope.friendName) < 0) {
            socket.emit('createRoom', $scope.groupName);
            socket.emit('joinRoom', {roomName: $scope.groupName, add: $scope.friendName});
        }
        $scope.friendName = $scope.groupName;
        currentRoom = $scope.groupName;
        $scope.path = "images/group.jpg";
        console.log("auto hide");
        $scope.showInputName = { 'display': 'none'};
    }
    socket.on('fromroom',function (data) {
        if(messages[data.to])
        {
            messages[data.to].push({msg: data.from +": " + data.msg, from: "othersMessage"});
        }
        else {
            messages[data.to]  = new Array();
            messages[data.to].push({msg:data.from +": " + data.msg, from: "othersMessage"});
        }
        $scope.$apply(function () {
            $scope.msglist = messages[$scope.friendName];
        });
        if($scope.friendName != [data.to])
        {
            if(badges[data.to])
            {
                badges[data.to]++;
            }
            else {
                badges[data.to] = 1;
            }
            $scope.$apply(function () {
                $scope.badges = badges;
            });
        }
    });
        socket.on('participants',function (data) {
            $scope.$apply(function () {
               $scope.participants = "";
                for(pName in data)
                {
                    $scope.participants += data[pName]+" ";
                }
            });
        });
});