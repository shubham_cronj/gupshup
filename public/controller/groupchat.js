app.controller('groupchat', function ($scope,id,messages,badges,$state,id) {
    $scope.block = 'none';
    $scope.username = id.username;
    // $scope.sideHead = 'Online Users';
    $scope.msglist = messages["group"];
    console.log("in chat control");
    socket.emit('giveUserList',"hi");
    var displayList = new Array();
    socket.on('get users', function (data) {
        updateOnlineUsers(data);
        console.log("listened to get users "+data);
    });
    function updateOnlineUsers(data) {
        $scope.$apply(function () {
            displayList = data;
            $scope.users = displayList;
        });
    }
    $scope.add = function(friendName) {
        if($scope.participants)
        $scope.participants += friendName+" ";
        else $scope.participants = friendName+", ";
        $scope.path = "images/group.jpg";
        socket.emit('joinRoom',{roomName:'room1', add:friendName});
    }
    socket.on('fromroom',function (data) {
        $scope.$apply(function () {
            $scope.msglist = messages['group'];
            });
    });
    socket.on('msg',function(data) {
        console.log(data.from + " - " + data.msg);
    });
    $scope.sendToRoom = function () {
        console.log('sent');
        if($scope.msgToSend) {
            socket.emit('toRoom', {msg: $scope.msgToSend, from: id.username});
            if (messages['group']) {
                messages['group'].push({msg: $scope.msgToSend, from: "myMessage"});
            }
            else {
                messages['group'] = new Array();
                messages['group'].push({msg: $scope.msgToSend, from: "myMessage"});
            }
            $scope.msglist = messages['group'];
        }
        $scope.msgToSend = '';
        $scope.participants = "GroupChat";
        $scope.path = "images/group.jpg";
    }

});