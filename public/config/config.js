app.config(function ($stateProvider) {
    $stateProvider
        .state('login',{
            url:'',
            templateUrl:'./templates/login.html',
            controller:'login'
        })
        .state('chat',{
            url:'/chatroom',
            templateUrl: './templates/chat.html',
            controller:'chat',
            controllerAs: 'chatAs'
        })
        .state('groupchat',{
            url:'/chatroom/group',
            templateUrl: './templates/groupchat.html',
            controller:'groupchat',
            controllerAs: 'groupchatAs'
        });

});